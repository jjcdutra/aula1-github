package entities;

public class Student {

	public String name;
	public double n1;
	public double n2;
	public double n3;
	
	public double notaFinal() {
		return n1 + n2 + n3;
	}
	
	public String toString() {
		String mensagem;
		if (notaFinal() > 60) {
			mensagem = "NOTA FINAL = " + notaFinal() + " APROVADO";
		} else {
			mensagem = "NOTA FINAL = " + notaFinal() + " REPROVADO " + " FALTAM " + (60 - notaFinal());
		}
		
		return mensagem;
	}
}
