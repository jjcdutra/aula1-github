package entities;

public class Account {

	private int accountNumer;
	private String holder;
	private double balance;
	
	public Account() {}
	
	public Account(int accountNumer, String holder) {
		this.accountNumer = accountNumer;
		this.holder = holder;
	}
	
	public Account(int accountNumer, String holder, double initialValue) {
		this.accountNumer = accountNumer;
		this.holder = holder;
		deposit(initialValue);
	}

	public int getAccountNumer() {
		return accountNumer;
	}

	public String getTHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void deposit(double value) {
		balance += value;
	}
	
	public void withdrawal(double value) {
		balance -= (value + 5);
	}
	
	public String toString() {
		return "Account "
				+ accountNumer
				+ ", Holder: "
				+ holder
				+ ", Balance $ "
				+ String.format("%.2f", balance);
	}
}
